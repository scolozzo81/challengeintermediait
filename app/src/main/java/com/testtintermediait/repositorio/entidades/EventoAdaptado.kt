package com.testtintermediait.repositorio.entidades

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import com.testtintermediait.repositorio.servicios.pojos.Comic
import com.testtintermediait.repositorio.servicios.pojos.Dates
import com.testtintermediait.repositorio.servicios.pojos.Thumbnail
import com.testtintermediait.repositorio.servicios.pojos.Urls

data class EventoAdaptado(
    val id: Int,
    val title: String?,
    val description: String?,
    val resourceURI: String?,
    val urls: List<Urls>?,
    val start: String?,
    val end: String?,
    val thumbnail: Thumbnail?,
    var createdAt: String = "0",
    var comics: ArrayList<Comic>?
)