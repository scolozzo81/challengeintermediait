package com.testtintermediait.repositorio.servicios

import com.testtintermediait.repositorio.servicios.interfaces.APIService
import com.testtintermediait.repositorio.servicios.interfaces.NetworkService
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseCharacteres
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseComicsXCharacter
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseComicsXEvent
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseEvents
import javax.inject.Inject

open class Service @Inject constructor(private val service: APIService): NetworkService {

    override suspend fun getCharacters(offfset: Int, limit:Int): ResponseCharacteres{
        return service.getCharacters(offfset,limit)
    }

    override suspend fun getEvents(limit: Int,offset:Int): ResponseEvents {
        return service.getEvents(limit,offset)
    }

    override suspend fun getComicsByCharacter(characterId: String): ResponseComicsXCharacter {
        return service.getComicsByCharacter(characterId)
    }

    override suspend fun getComicsByEvent(eventId: String): ResponseComicsXEvent {
        return service.getComicsByEvent(eventId)
    }

}