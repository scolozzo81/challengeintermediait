package com.testtintermediait.repositorio.servicios.interfaces

import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseCharacteres
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseComicsXCharacter
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseComicsXEvent
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseEvents
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIService {
    companion object {
        private const val COMICS_BY_CHARACTER_RELATIVE_URL = "characters/{characterId}/comics"
        private const val COMICS_BY_EVENT_RELATIVE_URL = "events/{eventId}/comics"
        private const val EVENTS__RELATIVE_URL = "events"
        private const val CHARACTERS__RELATIVE_URL = "characters"
    }

    @GET("$CHARACTERS__RELATIVE_URL")
    suspend  fun getCharacters(@Query("offset") offset: Int = 1, @Query("limit") limit: Int =15): ResponseCharacteres

    @GET("$EVENTS__RELATIVE_URL")
    suspend  fun getEvents(@Query("limit") limit: Int ,@Query("offset") offset: Int ): ResponseEvents

    @GET("$COMICS_BY_CHARACTER_RELATIVE_URL")
    suspend  fun getComicsByCharacter(@Path("characterId") characterId: String): ResponseComicsXCharacter

    @GET("$COMICS_BY_EVENT_RELATIVE_URL")
    suspend fun getComicsByEvent(@Path("eventId") eventId: String): ResponseComicsXEvent



}