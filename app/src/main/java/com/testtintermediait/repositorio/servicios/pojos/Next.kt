package com.testtintermediait.repositorio.servicios.pojos

import com.google.gson.annotations.SerializedName

data class Next(
    @SerializedName("resourceURI") val resourceURI : String,
    @SerializedName("name") val name : String
)
