package com.example.rappitest.api.gestionerrores

/**
 * Enum about status
 * of Contenedor
 */
enum class EstadoRespuesta {
    SUCCESS,
    ERROR,
    LOADING
}