package com.testtintermediait.repositorio.servicios.pojos

import com.google.gson.annotations.SerializedName

data class Dates(
    @SerializedName("type") val type : String,
    @SerializedName("date") val date : String
)
