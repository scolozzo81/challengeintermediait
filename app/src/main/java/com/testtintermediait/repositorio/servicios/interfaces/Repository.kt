package com.testtintermediait.repositorio.servicios.interfaces

import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.repositorio.servicios.pojos.Comic
import com.testtintermediait.repositorio.db.entidades.Event

interface Repository {
    suspend fun loadCharacters(offset: Int,limit:Int): ArrayList<Character>
    suspend fun getEvents(): List<Event>
    suspend fun getComicsByCharacter(characterId:String): ArrayList<Comic>
    suspend fun getComicsByEvent(eventId: String): ArrayList<Comic>
    suspend fun getCharacterById(idCharacter: String) :Character
}