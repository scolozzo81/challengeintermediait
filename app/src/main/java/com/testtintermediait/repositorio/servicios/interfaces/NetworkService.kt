package com.testtintermediait.repositorio.servicios.interfaces

import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseCharacteres
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseComicsXCharacter
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseComicsXEvent
import com.testtintermediait.repositorio.servicios.pojos.principales.ResponseEvents

interface NetworkService {
    suspend fun getCharacters(offfset: Int =1,limit:Int=15): ResponseCharacteres
    suspend fun getEvents(limit:Int,offset:Int): ResponseEvents
    suspend fun getComicsByCharacter(characterId: String): ResponseComicsXCharacter
    suspend fun getComicsByEvent(eventId: String): ResponseComicsXEvent

}