package com.testtintermediait.repositorio.servicios.pojos

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.testtintermediait.repositorio.servicios.pojos.Dates
import com.testtintermediait.repositorio.servicios.pojos.Thumbnail

data class Comic(
    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String?,
    @SerializedName("description") val description : String,
    @SerializedName("dates") val dates : List<Dates>?,

)
