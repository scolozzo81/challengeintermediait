package com.testtintermediait.repositorio.servicios.pojos.principales

import com.google.gson.annotations.SerializedName
import com.testtintermediait.repositorio.db.entidades.Character

data class CharacterResultData(
    @SerializedName("offset") val offset : Int,
    @SerializedName("limit") val limit : Int,
    @SerializedName("total") val total : Int,
    @SerializedName("count") val count : Int,
    @SerializedName("results") val characters : List<Character>
)
