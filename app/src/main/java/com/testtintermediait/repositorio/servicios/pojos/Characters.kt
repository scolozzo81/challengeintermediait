package com.testtintermediait.repositorio.servicios.pojos

data class Characters(
    val available : Int,
    val collectionURI : String,
    val items : List<String>,
    val returned : Int
)
