package com.example.rappitest.api.gestionerrores

enum class TiposErrorAPI {
    EXCEPCION_HTTP,
    ERROR_GENERAL
}
