package com.testtintermediait.repositorio.db.entidades

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.testtintermediait.repositorio.servicios.pojos.Comic
import com.testtintermediait.repositorio.servicios.pojos.Thumbnail
import com.testtintermediait.repositorio.servicios.pojos.Urls

@Entity(primaryKeys = ["id"])
data class Event(
    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String?,
    @SerializedName("description") val description : String?,
    @SerializedName("resourceURI") val resourceURI : String?,
    @SerializedName("urls") val urls : List<Urls>?,
    @SerializedName("start") val start : String?,
    @SerializedName("end") val end : String?,
    @SerializedName("thumbnail") val thumbnail : Thumbnail?,
    @ColumnInfo(name = "created_at")
    var createdAt :String="0",
)
