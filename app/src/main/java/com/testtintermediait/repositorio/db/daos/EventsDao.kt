package com.testtintermediait.repositorio.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testtintermediait.repositorio.db.entidades.Event

@Dao
interface EventsDao {
    @Query("SELECT * FROM Event")
    suspend fun getAllEvents(): List<Event>?

    @Query("SELECT COUNT(*) FROM Event")
    suspend fun getAmountEvents(): Long

    @Query("SELECT MAX(Event.created_at) FROM Event")
    suspend fun getLastDateEvents():String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllEvents(list: List<Event>)
}