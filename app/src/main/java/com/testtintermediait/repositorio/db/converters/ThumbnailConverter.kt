package com.testtintermediait.repositorio.db.converters

import androidx.room.TypeConverter
import com.testtintermediait.repositorio.servicios.pojos.Thumbnail
import org.json.JSONObject

class ThumbnailConverter {
    @TypeConverter
    fun fromSource(thumbnail: Thumbnail): String{
        return JSONObject().apply {
            put("path", thumbnail.path)
            put("extension", thumbnail.extension)
        }.toString()
    }

    @TypeConverter
    fun toThumbnail(sThumbnail: String): Thumbnail {
        val json = JSONObject(sThumbnail)
        return Thumbnail(json.getString("path"), json.getString("extension"))
    }
}