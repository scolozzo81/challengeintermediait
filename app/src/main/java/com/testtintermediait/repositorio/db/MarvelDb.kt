package com.testtintermediait.repositorio.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.testtintermediait.repositorio.db.converters.ThumbnailConverter
import com.testtintermediait.repositorio.db.converters.UrlsConverter
import com.testtintermediait.repositorio.db.daos.CharactersDao
import com.testtintermediait.repositorio.db.daos.EventsDao
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.repositorio.db.entidades.Event

@Database(entities = [Character::class,Event::class ],version = 1, exportSchema = false )
@TypeConverters(*[ThumbnailConverter::class, UrlsConverter::class])
abstract class MarvelDb : RoomDatabase() {
    abstract fun charactersDAO(): CharactersDao
    abstract fun eventsDAO(): EventsDao
}