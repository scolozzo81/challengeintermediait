package com.testtintermediait.repositorio.db.entidades

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.testtintermediait.repositorio.servicios.pojos.Thumbnail

@Entity(primaryKeys = ["id"])
data class Character(
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("description") val description : String,
    @SerializedName("thumbnail") val thumbnail : Thumbnail,
    @ColumnInfo(name = "created_at")
      var createdAt :String="0",
)
