package com.testtintermediait.repositorio.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.testtintermediait.repositorio.servicios.pojos.Urls
import org.json.JSONObject
import java.lang.reflect.Type


class UrlsConverter {
    @TypeConverter
    fun fromSource(urls: Urls): String{
        return JSONObject().apply {
            put("type", urls.type)
            put("url", urls.url)
        }.toString()
    }

    @TypeConverter
    fun toThumbnail(sUrls: String): Urls {
        val json = JSONObject(sUrls)
        return Urls(json.getString("type"), json.getString("url"))
    }

    @TypeConverter
    fun listToJson(value: List<Urls>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Urls>::class.java).toList()

}