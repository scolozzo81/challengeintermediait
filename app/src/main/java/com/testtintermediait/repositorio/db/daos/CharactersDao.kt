package com.testtintermediait.repositorio.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testtintermediait.repositorio.db.entidades.Character
import java.util.ArrayList

@Dao
interface CharactersDao {

    @Query("SELECT * FROM Character")
    suspend fun getAllCharacters(): List<Character>?

    @Query("SELECT COUNT(*) FROM Character")
    suspend fun getAmountCharacters(): Long

    @Query("SELECT * FROM Character limit :init, :end")
    suspend fun getCharactersByRange(init: Int, end: Int): List<Character>

    @Query("SELECT MAX(Character.created_at) FROM Character")
    suspend fun getLastDateCharacters():String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCharacters(list: ArrayList<Character>)

    @Query("SELECT * FROM Character WHERE Character.id=:idCharacter limit 1")
    suspend fun getCharacterById(idCharacter: String) :Character
}
