package com.testtintermediait.repositorio


import android.text.format.DateUtils
import com.example.rappitest.api.gestionerrores.EstadoRespuesta
import com.example.rappitest.api.gestionerrores.Gestor
import com.testtintermediait.repositorio.db.MarvelDb
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.repositorio.servicios.pojos.Comic
import com.testtintermediait.repositorio.db.entidades.Event
import com.testtintermediait.repositorio.servicios.interfaces.NetworkService
import com.testtintermediait.repositorio.servicios.interfaces.Repository
import com.testtintermediait.utils.ComrpobarConexion
import com.testtintermediait.utils.getFilterEvent
import com.testtintermediait.utils.setDateCharacters
import com.testtintermediait.utils.setDateEvent

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import javax.inject.Inject



class RepositoryImp @Inject constructor(
    private val db: MarvelDb,
    private val service: NetworkService,
    private val comprobarConexion: ComrpobarConexion
) : Repository {
    var charactersTotal = 1000000
    var eventsTotal = 1000000
    var enventAsked = 0

    override suspend fun loadCharacters(offset: Int, limit: Int): ArrayList<Character> {
        var characters: ArrayList<Character>
        var cacheOutDate: Boolean
        var thereAreCharacters: Boolean
        var charactersInCache: Boolean
        var newestDateCharacters = 0L
        var sizeCharacteresTable = 0L
        sizeCharacteresTable = db.charactersDAO().getAmountCharacters()
        runBlocking {
            characters = ArrayList<Character>()
            newestDateCharacters =
                if (sizeCharacteresTable == 0L) 0 else db.charactersDAO().getLastDateCharacters().toLong()
            thereAreCharacters = sizeCharacteresTable > 0
            charactersInCache = offset < db.charactersDAO().getAmountCharacters()
            cacheOutDate = newestDateCharacters <= 0 || !DateUtils.isToday(newestDateCharacters)
            characters = when {
                offset == 0 && thereAreCharacters && !cacheOutDate ->
                    db.charactersDAO().getCharactersByRange(1, limit) as ArrayList<Character>
                offset == 0 ->
                    getCharactersAPI()
                offset + limit <= charactersTotal && charactersInCache && !cacheOutDate -> db.charactersDAO()
                    .getCharactersByRange(offset, limit) as ArrayList<Character>
                offset < charactersTotal && charactersInCache && !cacheOutDate -> db.charactersDAO()
                    .getCharactersByRange(offset, charactersTotal - limit) as ArrayList<Character>
                offset + limit <= charactersTotal -> getCharactersAPI(offset, limit)
                offset < charactersTotal || true ->
                    getCharactersAPI(offset, charactersTotal - offset)
                else -> ArrayList<Character>()
            }
        }
        return characters
    }

    override suspend fun getEvents(): List<Event> {
        var events: ArrayList<Event>
        var cacheOutDate: Boolean
        var thereAreEvents: Boolean
        var newestDateEvents = 0L
        var sizeEventsTable = 0L
        sizeEventsTable = db.eventsDAO().getAmountEvents()
        events = ArrayList<Event>()
        newestDateEvents = if (sizeEventsTable == 0L) 0 else db.eventsDAO().getLastDateEvents().toLong()
        thereAreEvents = sizeEventsTable > 0
        cacheOutDate = newestDateEvents <= 0 || !DateUtils.isToday(newestDateEvents)
        runBlocking {
            events = when {
                thereAreEvents && !cacheOutDate && (enventAsked == eventsTotal || sizeEventsTable>24) ->
                    withContext(Dispatchers.Default) {
                        db.eventsDAO().getAllEvents() as ArrayList<Event>
                    }
                else ->
                    withContext(Dispatchers.Default) { getEventsFromAPI() }
            }
        }
        return if (events.size == 25 || enventAsked == eventsTotal)
            events
        else
                getEvents()
    }

    override suspend fun getComicsByCharacter(characterId: String): ArrayList<Comic> {
        var comics = ArrayList<Comic>()
        if (comprobarConexion.connectionIsAvailable()) {
            try {

                runBlocking {
                    var respuesta = Gestor.handleSuccess(service.getComicsByCharacter(characterId))
                    when (respuesta.status) {
                        EstadoRespuesta.SUCCESS -> {
                            respuesta.data?.data?.let { result ->
                                if (!result.comics.isNullOrEmpty())
                                    comics = result.comics as ArrayList<Comic>
                            }
                        }
                        else -> comics = ArrayList<Comic>()
                    }
                }
            } catch (e: Exception) {
            }
        }
        return comics
    }

    override suspend fun getComicsByEvent(eventId: String): ArrayList<Comic> {
        var comics = ArrayList<Comic>()
        runBlocking {
        if (comprobarConexion.connectionIsAvailable()) {
            try {
              var respuesta = Gestor.handleSuccess(service.getComicsByEvent(eventId))
                    when (respuesta.status) {
                        EstadoRespuesta.SUCCESS -> {
                            respuesta.data?.data?.let { result ->
                                if (!result.comics.isNullOrEmpty())
                                    comics = result.comics as ArrayList<Comic>
                            }
                        }
                        else -> comics = ArrayList<Comic>()
                    }
                }catch (e: Exception) {}
            }
        }
        return comics
    }

    override suspend fun getCharacterById(idCharacter: String): Character {
        return db.charactersDAO().getCharacterById(idCharacter)
    }

    private suspend fun getCharactersAPI(offfset: Int = 0, limit: Int = 15): ArrayList<Character> {
        var characters = ArrayList<Character>()
        runBlocking {
        if (comprobarConexion.connectionIsAvailable()) {
                try {
                    var respuesta = Gestor.handleSuccess(service.getCharacters(offfset, limit))
                    when (respuesta.status) {
                        EstadoRespuesta.SUCCESS -> {
                            respuesta.data?.data?.let { result ->
                                if (!result.characters.isNullOrEmpty()) {
                                    characters = result.characters as ArrayList<Character>
                                    db.charactersDAO()
                                        .insertAllCharacters(characters.setDateCharacters())
                                }
                                if (charactersTotal == 1000000) charactersTotal = result.total
                            }
                        }
                        else -> characters = ArrayList<Character>()
                    }
                } catch (e: Exception) {
                }
            }
        }
        return characters
    }

    private suspend fun getEventsFromAPI(): ArrayList<Event> {
        var events :ArrayList<Event>
        val diff = eventsTotal - enventAsked
        val limit = if (diff >= 25) 25 else diff
        runBlocking {
            if (comprobarConexion.connectionIsAvailable()) {
               try {
                    var response = withContext(Dispatchers.Default) {
                        Gestor.handleSuccess(
                            service.getEvents(
                                limit,
                                enventAsked
                            )
                        )
                    }
                    when (response.status) {
                        EstadoRespuesta.SUCCESS -> {
                            response.data?.data?.let { result ->
                                if (eventsTotal == 1000000)
                                    eventsTotal = result.total
                                if (!result.events.isNullOrEmpty()) {
                                    events = (result.events as ArrayList<Event>).setDateEvent()
                                    enventAsked += events.size
                                    db.eventsDAO().insertAllEvents(events.getFilterEvent())
                                }
                            }
                        }
                        else -> events = ArrayList()
                    }
               } catch (e: Exception) {}
            }
        }
        return db.eventsDAO().getAllEvents() as ArrayList<Event>
    }

}



