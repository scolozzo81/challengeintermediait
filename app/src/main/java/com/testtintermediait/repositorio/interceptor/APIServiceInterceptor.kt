package com.testtintermediait.repositorio.interceptor

import com.testtintermediait.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class APIServiceInterceptor @Inject constructor() : Interceptor   {

    companion object {
        private const val PUBLIC_API_KEY: String = "apikey"
        private const val HSH_MD5: String = "hash"
        private const val TIME_OUT: String = "ts"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val originalHttpUrl: HttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(PUBLIC_API_KEY, BuildConfig.PUBLIC_API_KEY)
            .addQueryParameter(HSH_MD5, BuildConfig.HASH_MD5)
            .addQueryParameter(TIME_OUT, BuildConfig.TIME_OUT)
            .build()

        val requestBuilder: Request.Builder = original.newBuilder()
            .url(url)

        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }

}