package com.testtintermediait.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testtintermediait.repositorio.servicios.interfaces.Repository
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.repositorio.servicios.pojos.Comic
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class DetailCharacterViewModel  @Inject constructor(private val repository: Repository) : ViewModel(){
    val comicsByCharacter= MutableLiveData<List<Comic>>()
    val character = MutableLiveData<Character>()

    fun getComicsBycharacter(characterId: String) {
        viewModelScope.launch {
            comicsByCharacter.apply {
                 value =  repository.getComicsByCharacter(characterId)
            }
        }
    }
    fun loadCharacterById(idCharacter: String){
        viewModelScope.launch{
            character.apply {
                value =  repository.getCharacterById(idCharacter)
            }
        }
    }


}