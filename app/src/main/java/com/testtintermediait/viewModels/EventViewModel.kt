package com.testtintermediait.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testtintermediait.repositorio.servicios.interfaces.Repository
import com.testtintermediait.repositorio.servicios.pojos.Comic
import com.testtintermediait.repositorio.entidades.EventoAdaptado
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.testtintermediait.utils.transformEvent

@HiltViewModel
class EventViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    var events = MutableLiveData<List<EventoAdaptado>>()

    fun getEvents() {
        viewModelScope.launch {
            events.value.apply {
                events.value = repository.getEvents().sortedBy { it.start }.transformEvent()
            }
        }
    }

    fun getComicsByEvent(eventId: String) {
        viewModelScope.launch {
            var comicList = repository.getComicsByEvent(eventId)
            events.value = updateEvents(eventId,comicList)
        }
    }

    private fun updateEvents(eventId: String, comicList: ArrayList<Comic>): List<EventoAdaptado>? {
       var list = ArrayList<EventoAdaptado>()
        events.value?.forEach { it ->
            if (eventId.equals(it.id.toString())) it.comics = comicList
            list.add(it)
        }
        return list
    }


}