package com.testtintermediait.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testtintermediait.repositorio.servicios.interfaces.Repository
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.repositorio.entidades.EventoAdaptado
import com.testtintermediait.repositorio.servicios.pojos.Comic
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(private val repository: Repository) : ViewModel(){
    val characters = MutableLiveData<List<Character>>()
    val LIMIT=15

    fun loadCharacters() {
        viewModelScope.launch {
                var offset = if (characters.value.isNullOrEmpty()) 0 else characters.value!!.size
                var list = repository.loadCharacters(offset, LIMIT)
                characters.value = updateCharcaters(list)
        }
    }

      fun updateCharcaters(charcatersList: ArrayList<Character>): ArrayList<Character> {
        var list= ArrayList<Character>()
          characters.value?.let { list.addAll(it) }
          list?.addAll(charcatersList)
        return list
    }

}
