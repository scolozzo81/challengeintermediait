package com.testtintermediait.vistas

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.testtintermediait.databinding.FragmentLoginBinding


class Login : Fragment() {
   private lateinit var binding : FragmentLoginBinding
     private lateinit var activity: BottomToolBar
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
          activity= getActivity() as BottomToolBar
        binding = FragmentLoginBinding.inflate(layoutInflater)
        binding.btnLogin.setOnClickListener{
            val action = LoginDirections.actionLoginToCharacters()
            findNavController().navigate(action)
        }

        return binding.root
    }
   override fun onResume() {
      super.onResume()
       activity.hideBottomToolbar()
    }

}