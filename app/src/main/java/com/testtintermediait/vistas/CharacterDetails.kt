package com.testtintermediait.vistas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.testtintermediait.R
import com.testtintermediait.databinding.ActivityCharacterDetailsBinding
import com.testtintermediait.repositorio.servicios.pojos.Comic
import com.testtintermediait.vistas.adaptadores.ComicsAdapter
import com.testtintermediait.viewModels.DetailCharacterViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class CharacterDetails : AppCompatActivity() {
    private lateinit var adapter: ComicsAdapter
    private lateinit var binding: ActivityCharacterDetailsBinding
    private val viewModel: DetailCharacterViewModel by viewModels()
    private var comicsList = ArrayList<Comic>()
    private var idCharacter = "0"
    val args: CharacterDetailsArgs by navArgs()
    private lateinit var layoutM: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idCharacter = args.idComic
        binding = ActivityCharacterDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.loadCharacterById(idCharacter)
        setLayout()
        setListeners()
        setObservers()
    }

    private fun setLayout() {
        binding.charactersDetailRecycler.apply {
            layoutM = LinearLayoutManager(binding.root.context)
            this.layoutManager=layoutM
        }
    }

    private fun setListeners() {
        binding.toolbarCharacterDetail.setNavigationOnClickListener {
            finish()
        }
    }

    private fun setObservers() {
        viewModel.character.observe(this, Observer {
            binding.txtCharactersDetailNom.text = it.name.toUpperCase()
            binding.tvDescriptionCharacter.text = it.description
            it.thumbnail.path.let { path ->
                it.thumbnail.extension.let { ext ->
                    val url = "$path.$ext"
                    Picasso.get()
                        .load(url)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(binding.imgCharacterDetail)
                }
            }
            viewModel.getComicsBycharacter(idCharacter)
        })
        viewModel.comicsByCharacter.observe(this, Observer {
            comicsList.addAll(it)
            if (binding.charactersDetailRecycler.adapter == null) {
                adapter = ComicsAdapter(comicsList)
                binding.charactersDetailRecycler.adapter = adapter
            } else
                adapter.notifyDataSetChanged()
        })
    }
}