package com.example.a3_fragment_navigation_marvel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.testtintermediait.R
import com.testtintermediait.databinding.FragmentPrimerBinding
import com.testtintermediait.vistas.interfaces.OnClickCharacter
import com.testtintermediait.viewModels.ListViewModel
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.vistas.BottomToolBar
import com.testtintermediait.vistas.adaptadores.CharacterAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CharactersFragment : Fragment(), OnClickCharacter {
    private lateinit var activity: BottomToolBar
    private var isLoading: Boolean = true
    private lateinit var binding:FragmentPrimerBinding
    private lateinit var characterAdapter : CharacterAdapter

    private val viewModel: ListViewModel by viewModels()

    private var characters = ArrayList<Character>()
    private lateinit var layoutM: LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPrimerBinding.inflate(layoutInflater)
        activity= getActivity() as BottomToolBar
        characterAdapter = CharacterAdapter(characters, this)
        binding.charactersRecycler.apply {
            layoutM = LinearLayoutManager(binding.root.context)
             this.layoutManager=layoutM

        }

        viewModel.loadCharacters()
        setObservers()
        setListeners()
        return binding.root
    }

    private fun setListeners() {
        binding.charactersRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    val visibleItemCount = layoutM.childCount
                    val pastVisibleItem = layoutM.findFirstCompletelyVisibleItemPosition()
                    val total = characterAdapter.itemCount
                    if (!isLoading)
                        if (visibleItemCount + pastVisibleItem >= total) {
                            if (!isLoading){
                                isLoading = true
                                binding.progressBar.visibility = View.VISIBLE
                                viewModel.loadCharacters()
                            }
                        }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    private fun setObservers() {
        viewModel.characters.observe(this.viewLifecycleOwner, Observer {
          it?.let {
              characters.addAll(it)
          }
            if ( binding.charactersRecycler.adapter == null) {
                characterAdapter = CharacterAdapter(characters, this)
                binding.charactersRecycler.adapter = characterAdapter
             } else
                characterAdapter.notifyDataSetChanged()
            isLoading = false
            binding.progressBar.visibility = View.GONE
        })
    }

    override fun onClickCharacter(character: Character) {

       val action= CharactersFragmentDirections.actionPrimerFragmentToCharacterDetails(character.id.toString())
        findNavController().navigate(action)

    }

    fun navigate(){
        findNavController().navigate(R.id.action_primerFragment_to_eventFragment)
    }

    override fun onResume() {
        super.onResume()
        activity.showBottomToolbar()
    }
}