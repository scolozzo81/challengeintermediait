package com.testtintermediait.vistas.enums

enum class StateItemEvent {
    OPENED,
    CLOSED
}