package com.testtintermediait.vistas.interfaces

import com.testtintermediait.repositorio.db.entidades.Character

interface OnClickCharacter {
    fun onClickCharacter(character: Character)
}