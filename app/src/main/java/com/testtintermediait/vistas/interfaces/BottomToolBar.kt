package com.testtintermediait.vistas

interface BottomToolBar{
    fun hideBottomToolbar()
    fun showBottomToolbar()
}