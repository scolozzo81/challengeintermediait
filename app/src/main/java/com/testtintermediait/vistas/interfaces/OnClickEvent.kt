package com.testtintermediait.vistas.interfaces


interface OnClickEvent {
    fun onClickEvent(idEvent: Int,pos: Int)
}