package com.testtintermediait.vistas


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.a3_fragment_navigation_marvel.CharactersFragment
import com.testtintermediait.R
import dagger.hilt.android.AndroidEntryPoint
import com.testtintermediait.databinding.ActivityListsBinding

@AndroidEntryPoint
class ListsActivity: AppCompatActivity(), BottomToolBar {
    private lateinit var navController: NavController
    private lateinit var binding:ActivityListsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityListsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController = findNavController(R.id.fragment_container_view)


        binding.ivEvent.setOnClickListener() {
            supportFragmentManager.fragments.forEach {
                it.childFragmentManager.fragments.forEach { fragment ->
                    if (fragment is CharactersFragment) {
                        binding.ivEvent.setImageResource(R.drawable.ic_calendar_selected)
                        binding.ivCharacters.setImageResource(R.drawable.ic_person)
                        fragment.navigate()
                    }
                }
            }
        }

        binding.ivCharacters.setOnClickListener() {
            supportFragmentManager.fragments.forEach {
                it.childFragmentManager.fragments.forEach { fragment ->
                    if (fragment is EventFragment) {
                        binding.ivEvent.setImageResource(R.drawable.ic_calendar)
                        binding.ivCharacters.setImageResource(R.drawable.ic_person_selected)
                        fragment.navigate()
                    }
                }
            }
        }

    }

    override fun onBackPressed() {}

    override fun hideBottomToolbar() {
        binding.toolbar2.visibility = GONE
    }

    override fun showBottomToolbar() {
        binding.toolbar2.visibility = VISIBLE
    }

}