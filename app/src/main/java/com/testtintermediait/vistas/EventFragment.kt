package com.testtintermediait.vistas

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import com.testtintermediait.R
import com.testtintermediait.databinding.FragmentEventBinding
import com.testtintermediait.repositorio.db.entidades.Event
import com.testtintermediait.repositorio.entidades.EventoAdaptado
import com.testtintermediait.viewModels.EventViewModel
import com.testtintermediait.vistas.adaptadores.CharacterAdapter
import com.testtintermediait.vistas.adaptadores.EventAdapter
import com.testtintermediait.vistas.interfaces.OnClickEvent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EventFragment : Fragment(), OnClickEvent {
    private lateinit var binding: FragmentEventBinding
    private lateinit var activity: BottomToolBar
    private val viewModel: EventViewModel by viewModels()
    private lateinit var eventLayoutManager: LayoutManager
    private lateinit var eventAdapter: EventAdapter
    private var isLoading = true
    private var eventList = ArrayList<EventoAdaptado>()
    private var position = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEventBinding.inflate(layoutInflater)
        activity = getActivity() as BottomToolBar
        setLayout()
        setObservers()
        viewModel.getEvents()
        return binding.root
    }

    private fun setObservers() {
        viewModel.events.observe(this.viewLifecycleOwner, Observer {

            if (binding.eventRecycler.adapter == null) {
                eventList.addAll(it)
                eventAdapter = EventAdapter(eventList, this)
                binding.eventRecycler.adapter = eventAdapter
            } else if(position>=0){
                eventList.clear()
                eventList.addAll(it)
                binding.eventRecycler.adapter?.notifyItemChanged(position)
                position=-1
            }
            isLoading = false
            binding.progressBar.visibility = View.GONE
        })

    }

    private fun setLayout() {
        binding.eventRecycler.apply {
            eventLayoutManager = LinearLayoutManager(binding.root.context)
            this.layoutManager = eventLayoutManager
        }
    }

    override fun onResume() {
        super.onResume()
        activity.showBottomToolbar()
    }

    fun navigate() {
        findNavController().navigate(R.id.action_eventFragment_to_characters_fragment)
    }

    override fun onClickEvent(idEvent: Int, pos: Int) {
        isLoading = true
        binding.progressBar.visibility = View.VISIBLE
        position = pos
        viewModel.getComicsByEvent(idEvent.toString())
    }

}