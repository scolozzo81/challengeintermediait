package com.testtintermediait.vistas.adaptadores


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.testtintermediait.R
import com.testtintermediait.databinding.ItemEventBinding
import com.testtintermediait.repositorio.entidades.EventoAdaptado
import com.testtintermediait.utils.getDate
import com.testtintermediait.utils.getSDateFormated
import com.testtintermediait.vistas.enums.StateItemEvent
import com.testtintermediait.vistas.interfaces.OnClickEvent
import java.util.*

class EventAdapter(var eventList: MutableList<EventoAdaptado>, private val listener: OnClickEvent) :
    RecyclerView.Adapter<EventAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = eventList.get(position)

        holder.bindItem(event)
        holder.binding.ivIconEvent.setOnClickListener {
            when (holder.state) {
                StateItemEvent.CLOSED -> {
                    if (event.comics == null && !holder.comicsAsked) {
                        holder.state = StateItemEvent.OPENED
                        holder.binding.llComics.visibility = View.VISIBLE
                        holder.comicsAsked= true
                        listener.onClickEvent(event.id, position)
                    } else if (!event.comics.isNullOrEmpty()) {
                        holder.binding.tvTitleComicsEvents.text =
                            holder.context.getString(R.string.comics_to_discuss)
                        holder.state = StateItemEvent.OPENED
                        holder.binding.llComics.visibility = View.VISIBLE
                    }
                }
                else -> {
                    holder.state = StateItemEvent.CLOSED
                    holder.binding.llComics.visibility = View.GONE
                }
            }
        }
    }


    override fun getItemCount(): Int = eventList.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemEventBinding.bind(view)
        private lateinit var comicsAdapter: ComicsAdapter
        private lateinit var comicsLManager: LinearLayoutManager
        var state = StateItemEvent.CLOSED
        var context = view.getContext()
         var comicsAsked = false
        fun bindItem(event: EventoAdaptado) {
            var date = ""
            event.start?.let {
                date = it.getSDateFormated(context)
            }
            binding.tvNameEvent.text = event.title?.toUpperCase()
            binding.tvDescriptionEvent.text = date
            binding.itemRecycler.apply {
                comicsLManager = LinearLayoutManager(binding.root.context)
                this.layoutManager = comicsLManager
                event.comics?.let {
                    this.adapter = ComicsAdapter(it)
                }
            }
            event.thumbnail?.path.let { path ->
                event.thumbnail?.extension.let { ext ->
                    val url = "$path.$ext"
                    Picasso.get()
                        .load(url)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(binding.ivEvent)
                }
            }


        }
    }
}


