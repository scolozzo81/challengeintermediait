package com.testtintermediait.vistas.adaptadores

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.testtintermediait.R
import com.testtintermediait.databinding.ItemCharacterBinding
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.vistas.interfaces.OnClickCharacter


class CharacterAdapter(var charactersList: MutableList<Character>, private val listener: OnClickCharacter):
    RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_character, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = charactersList.get(position)
        holder.enlazarItem(character)

        holder.binding.cvCharacterItem.setOnClickListener{
             listener.onClickCharacter(character)
        }
    }

    override fun getItemCount(): Int = charactersList.size

     class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
       val binding = ItemCharacterBinding.bind(view)
        var contexto = view.context

        fun enlazarItem(character: Character) {
            binding.tvNameCharacter.text = character.name.toUpperCase()
            binding.tvDescriptionCharacter.text = character.description
            character.thumbnail.path.let { path ->
                character.thumbnail.extension.let { ext->
                    val url= "$path.$ext"
                    Picasso.get()
                        .load(url)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(binding.IMGPelicula)
                }
            }
        }
    }
}


