package com.testtintermediait.vistas.adaptadores

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testtintermediait.R
import com.testtintermediait.databinding.ItemComicsToDiscussBinding
import com.testtintermediait.repositorio.servicios.pojos.Comic


class ComicsAdapter (var comicsList: MutableList<Comic>):
    RecyclerView.Adapter<ComicsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comics_to_discuss, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val comic = comicsList.get(position)
        holder.enlazarItem(comic)

    }


    override fun getItemCount(): Int = comicsList.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val binding = ItemComicsToDiscussBinding.bind(view)
        fun enlazarItem(comic: Comic) {
            binding.tvNameComic.text= comic.title?.toUpperCase()
            binding.tvDescriptionComic.text = comic.dates?.filter { it-> it.type.equals("onsaleDate") }?.get(0)?.date?.subSequence(0,4)
            }
        }
    }



