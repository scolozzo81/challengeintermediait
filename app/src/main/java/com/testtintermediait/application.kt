package com.testtintermediait

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class application: Application() {

}