package com.testtintermediait.utils

import android.content.Context
import com.testtintermediait.R
import com.testtintermediait.repositorio.db.entidades.Character
import com.testtintermediait.repositorio.db.entidades.Event
import com.testtintermediait.repositorio.entidades.EventoAdaptado
import com.testtintermediait.repositorio.servicios.pojos.Comic
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Month
import java.time.ZoneId
import java.util.*


fun ArrayList<Character>.setDateCharacters(): ArrayList<Character> {
    this.forEach {
        it.createdAt = System.currentTimeMillis().toString()
    }
    return this
}

fun ArrayList<Event>.setDateEvent(): ArrayList<Event> {
    this.forEach {
        it.createdAt = System.currentTimeMillis().toString()
    }
    return this
}

fun ArrayList<Event>.getFilterEvent(): ArrayList<Event> {
    val f = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var eventosFiltrados = ArrayList<Event>()
    this.forEach {
        it.end?.let { date ->
            try {
                val d = f.parse(date).time
                if (d > System.currentTimeMillis())
                    eventosFiltrados.add(it)
            } catch (e: Exception) {
            }
        }
    }
    return eventosFiltrados
}

fun String?.getDate(): Long? {
    val f = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var response: Long? = null
    try {
        response = f.parse(this).time
    } catch (e: Exception) {
    }
    return response
}

fun List<Event>.transformEvent(): ArrayList<EventoAdaptado> {
    var comicAdaptadoList = ArrayList<EventoAdaptado>()
    this.forEach { evento ->
        comicAdaptadoList.add(
            EventoAdaptado(
                evento.id, evento.title, evento.description, evento.resourceURI, evento.urls,
                evento.start, evento.end, evento.thumbnail, evento.createdAt, null
            )
        )
    }
    return comicAdaptadoList
}

/***
 * get sMonth ddd, yyyy
 */
fun String.getSDateFormated(context: Context): String {
    val f = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var response: Date?
    var date = Calendar.getInstance()
    var months: Array<String> = context.resources.getStringArray(R.array.months)
    var month = ""
    var day = ""
    var year = ""
    var sDtate =""
    try {
        response = f.parse(this)
    } catch (e: Exception) {
        response = null
    }
    response?.let { response ->
        date.setTime(response)
        year = date.get(Calendar.YEAR).toString()
        day = date.get(Calendar.DAY_OF_MONTH).toString()
        month = when (date.get(Calendar.MONTH)) {
            0 -> months[0]
            1 -> months[1]
            2 -> months[2]
            3 -> months[3]
            4 -> months[4]
            5 -> months[5]
            6 -> months[6]
            7 -> months[7]
            8 -> months[8]
            9 -> months[9]
            10 -> months[10]
            11 -> months[11]
            else -> ""
        }
        sDtate= month + " " + day + "," + year
    }
    return sDtate
}

