package com.testtintermediait.id

import com.testtintermediait.repositorio.servicios.interfaces.Repository
import com.testtintermediait.repositorio.RepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun getRepository( implRepository: RepositoryImp
    ): Repository
}


