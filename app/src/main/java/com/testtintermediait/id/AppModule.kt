package com.testtintermediait.id

import android.app.Application


import androidx.room.Room
import com.testtintermediait.BuildConfig
import com.testtintermediait.repositorio.db.MarvelDb
import com.testtintermediait.repositorio.interceptor.APIServiceInterceptor
import com.testtintermediait.repositorio.servicios.interfaces.APIService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    companion object {
        val TIMEOUT= 20000
    }
    @Singleton
    @Provides
    fun getDatabase(app: Application): MarvelDb =
        Room.databaseBuilder(app, MarvelDb::class.java, "Marvel").build()

    @Provides
    fun provideInterceptor(): Interceptor {
        return APIServiceInterceptor()
    }

    @Provides
    fun getRetrofit(cliente : OkHttpClient): APIService = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(cliente)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(APIService::class.java)


    @Provides
    fun provideOkHttpClient(interceptor: APIServiceInterceptor,networkInterceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(networkInterceptor)
            .connectTimeout(TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .readTimeout(TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .followRedirects(true)
            .followSslRedirects(true)
            .addInterceptor(interceptor).build()

    @Provides
    fun provideNetworkInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

}