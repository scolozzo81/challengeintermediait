package com.testtintermediait.id

import com.testtintermediait.repositorio.servicios.interfaces.NetworkService
import com.testtintermediait.repositorio.servicios.Service
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkServiceModule {
    @Singleton
    @Binds
    abstract fun getNetworkService(
        implNetworkService: Service
    ): NetworkService
}